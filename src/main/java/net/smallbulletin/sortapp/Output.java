package net.smallbulletin.sortapp;

import com.google.common.collect.ImmutableList;

public class Output<E extends Comparable<E>> {

	private ImmutableList<E> sorted;
	private Measurements measurements;

	public Output() {

	}

	public Output(ImmutableList<E> sorted, Measurements measurements) {
		this.sorted = sorted;
		this.measurements = measurements;
	}

	public ImmutableList<E> getSorted() {
		return sorted;
	}

	public void setSorted(ImmutableList<E> sorted) {
		this.sorted = sorted;
	}

	public Measurements getMeasurements() {
		return measurements;
	}

	public void setMeasurements(Measurements measurements) {
		this.measurements = measurements;
	}

}
