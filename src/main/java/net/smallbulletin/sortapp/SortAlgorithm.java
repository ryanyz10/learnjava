package net.smallbulletin.sortapp;

import java.util.List;

public interface SortAlgorithm<E extends Comparable<E>> {
    Output<E> sort(List<E> input);
}
