package net.smallbulletin.sortapp.impl;

import java.util.List;

import net.smallbulletin.sortapp.Output;
import net.smallbulletin.sortapp.SortAlgorithm;
import net.smallbulletin.sortapp.SortAlgorithmName;
import net.smallbulletin.sortapp.util.MeasuredArray;

public class SelectionSort<E extends Comparable<E>> implements SortAlgorithm<E> {

    @Override
    public Output<E> sort(final List<E> input) {
        final MeasuredArray<E> sorted = new MeasuredArray<>(SortAlgorithmName.SELECTION_SORT.name(), input);
        int min;

        for (int i = 0; i < sorted.size() - 1; i++) {
            min = i;
            for (int j = i + 1; j < sorted.size(); j++) {
                if (sorted.compare(min, j) > 0) {
                    min = j;
                }

            }

            final E tmp = sorted.get(i);
            sorted.set(i, sorted.get(min));
            sorted.set(min, tmp);
        }

        final Output<E> output = new Output<>(sorted.asImmutableList(), sorted.getMeasurements());

        return output;
    }

}
