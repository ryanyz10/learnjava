package net.smallbulletin.sortapp.impl;

import java.util.List;

import net.smallbulletin.sortapp.Output;
import net.smallbulletin.sortapp.SortAlgorithm;
import net.smallbulletin.sortapp.SortAlgorithmName;
import net.smallbulletin.sortapp.util.MeasuredArray;

public class InsertionSort<E extends Comparable<E>> implements SortAlgorithm<E> {

    @Override
    public Output<E> sort(final List<E> input) {
        int j;
        final MeasuredArray<E> sorted = new MeasuredArray<>(SortAlgorithmName.INSERTION_SORT.name(), input);
        for (int i = 1; i < sorted.size(); i++) {

            final E tmp = sorted.get(i);
            j = i;

            while (j > 0 && sorted.compare(j - 1, tmp) >= 0) {
                sorted.set(j, sorted.get(j - 1));
                j--;
            }

            sorted.set(j, tmp);
        }

        final Output<E> output = new Output<>(sorted.asImmutableList(), sorted.getMeasurements());

        return output;
    }

}
