package net.smallbulletin.sortapp;

public enum SortAlgorithmName {

    BUBBLE_SORT, SELECTION_SORT, INSERTION_SORT, MERGE_SORT, SHELL_SORT, QUICK_SORT, RADIX_SORT;

}
