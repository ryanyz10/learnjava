package net.smallbulletin.sortapp;

public class Measurements {

	private final String sort;
	private int gets = 0;
	private int sets = 0;
	private int compares = 0;

	public Measurements(String sort) {
		super();
		this.sort = sort;
	}

	public void incGets() {
		gets++;
	}

	public void incSets() {
		sets++;
	}

	public void incCompares() {
		compares++;
	}

	public int getGets() {
		return gets;
	}

	public int getSets() {
		return sets;
	}

	public int getCompares() {
		return compares;
	}

	public String getSort() {
		return sort;
	}

}
