package net.smallbulletin.sortapp;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;

@Path("/sortapp")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SortAppResource {

	@POST
	@Timed
	public <E extends Comparable<E>> Output<E> sort(@DefaultValue("BUBBLE_SORT") @QueryParam("sort") final SortAlgorithmName sort,
			final Input<E> input) {
		final SortAlgorithm<E> impl = SortAlgorithmFactory.of(sort);
		final Output<E> output = impl.sort(input.getUnsorted());

		return output;
	}
}
