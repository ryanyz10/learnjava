package net.smallbulletin.listapp;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;

@Path("/listapp")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ListAppResource {

	@POST
	@Timed
	@Path("/josephus")
	public Output takeOut(Input input) {
		RemovePeople obj = new RemovePeople(input.getNames(), input.getSkip(), input.getStart());
		String str = obj.takeOut();
		ArrayList<String> removed = (ArrayList<String>) obj.getRemoved();
		Output output = new Output(input.getNames(), removed, str);

		return output;
	}
}
