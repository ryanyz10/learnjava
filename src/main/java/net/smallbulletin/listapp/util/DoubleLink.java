package net.smallbulletin.listapp.util;

public class DoubleLink<E> {
    private DoubleLink<E> next;
    private DoubleLink<E> prev;
    private final E value;

    public DoubleLink(E value) {
        this.value = value;
    }

    public DoubleLink(E value, DoubleLink<E> prev, DoubleLink<E> next) {
        this.value = value;
        this.prev = prev;
        this.next = next;
        prev.setNext(this);
        next.setPrev(this);
    }

    public DoubleLink<E> getNext() {
        return next;
    }

    public void setNext(DoubleLink<E> next) {
        this.next = next;
    }

    public DoubleLink<E> getPrev() {
        return prev;
    }

    public void setPrev(DoubleLink<E> prev) {
        this.prev = prev;
    }

    public E getValue() {
        return value;
    }

}
