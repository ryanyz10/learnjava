package net.smallbulletin.listapp.util;

import java.util.List;

public class CircularLinkedList<E> {
    private Link<E> prev = null;
    private int size = 0;

    public CircularLinkedList() {

    }

    public CircularLinkedList(final List<E> list) {
        for (final E e : list) {
            add(e);
        }
    }

    public void add(final E e) {
        final Link<E> toAdd = new Link<E>(e);

        if (this.prev == null) {
            toAdd.setNext(toAdd);
        } else {
            toAdd.setNext(this.prev.getNext());
            this.prev.setNext(toAdd);
        }

        this.prev = toAdd;
        this.size++;
    }

    public E removeCurr() {
        if (this.size == 1) {
            final E tmp = this.prev.getValue();
            this.prev = null;
            this.size--;
            return tmp;
        }

        final E tmp = this.prev.getNext().getValue();
        this.prev.setNext(this.prev.getNext().getNext());
        this.size--;
        return tmp;
    }

    public E getCurr() {
        return this.prev.getNext().getValue();
    }

    public E next() {
        this.prev = this.prev.getNext();

        return this.prev.getValue();
    }

    public int size() {
        return this.size;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        Link<E> tmp = this.prev;

        sb.append('[');

        for (int i = 0; i < this.size; i++) {
            tmp = tmp.getNext();
            sb.append(tmp.getValue());

            if (i != this.size - 1) {
                sb.append(", ");
            }
        }

        sb.append(']');
        return sb.toString();
    }

}
