package net.smallbulletin.main;

import net.smallbulletin.listapp.ListAppResource;
import net.smallbulletin.recursionapp.RecursionAppResource;
import net.smallbulletin.sortapp.SortAppResource;
import net.smallbulletin.stackapp.StackAppResource;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

public class MainApp extends Application<MainAppConfiguration> {

	public static void main(String[] args) throws Exception {

		new MainApp().run(args);
	}

	@Override
	public void run(MainAppConfiguration config, Environment env) throws Exception {
		SortAppResource sortapp = new SortAppResource();
		env.jersey().register(sortapp);
		StackAppResource stackapp = new StackAppResource();
		env.jersey().register(stackapp);
		ListAppResource listapp = new ListAppResource();
		env.jersey().register(listapp);
		RecursionAppResource recurseApp = new RecursionAppResource();
		env.jersey().register(recurseApp);
	}

}
