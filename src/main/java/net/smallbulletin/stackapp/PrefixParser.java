package net.smallbulletin.stackapp;

import net.smallbulletin.stackapp.util.MeasuredStack;
import net.smallbulletin.stackapp.util.TreeNode;

public class PrefixParser implements Parser {

	private final MeasuredStack<TreeNode<Character>> stack;

	public PrefixParser(Measurements measurements) {
		this.stack = new MeasuredStack<TreeNode<Character>>(measurements);
	}

	@Override
	public TreeNode<Character> parse(String str) {
		for (int i = 0; i < str.length(); i++) {
			char c = str.charAt(i);

			onChar(c);
		}

		TreeNode<Character> tree = stack.pop();

		if (stack.isEmpty()) {
			return tree;
		}

		throw new IllegalArgumentException("Not a complete prefix string: "
				+ str);
	}

	private void onChar(char c) {
		if (Character.isWhitespace(c)) {
			return;
		}

		if (Character.isLetterOrDigit(c) || isOperator(c)) {
			TreeNode<Character> node = new TreeNode<>(c);
			stack.push(node);
			while (collapseIfPossible()) {
				// keep collapsing the stack
			}

		} else {
			throw new IllegalArgumentException("Invalid character: " + c);
		}

	}

	public boolean collapseIfPossible() {
		if (stack.size() < 3) {
			return false;
		}

		TreeNode<Character> right = stack.peek();
		TreeNode<Character> left = stack.get(stack.size() - 2);
		TreeNode<Character> root = stack.get(stack.size() - 3);

		if (!isComplete(root) && isComplete(left) && isComplete(right)) {
			root.setLeft(left);
			root.setRight(right);
			stack.pop();
			stack.pop();
			return true;
		}

		return false;
	}

	public static boolean isComplete(TreeNode<Character> node) {
		if (isOperator(node)) {
			return !(node.getLeft() == null || node.getRight() == null);
		}

		return true;
	}

	public static boolean isOperator(TreeNode<Character> node) {
		char c = node.getValue();

		return isOperator(c);
	}

	public static boolean isOperator(char c) {
		return (c == '+' || c == '-' || c == '*' || c == '/');
	}

}
