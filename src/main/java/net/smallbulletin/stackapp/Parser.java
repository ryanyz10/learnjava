package net.smallbulletin.stackapp;

import net.smallbulletin.stackapp.util.TreeNode;

public interface Parser {
	TreeNode<Character> parse(String str);
}
