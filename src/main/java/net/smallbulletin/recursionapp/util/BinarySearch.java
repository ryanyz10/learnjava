package net.smallbulletin.recursionapp.util;

//precondition: sorted array
public class BinarySearch {
    public static int search(final String[] array, final String searchFor, final int lower, final int upper) {

        if (lower > upper) {
            return -1;
        }

        final int midIndex = (lower + upper) / 2;
        int search = searchFor.compareTo(array[midIndex]);

        if (search == 0) {
            return midIndex;
        } else if (search > 0) {
            return search(array, searchFor, midIndex + 1, upper);
        } else {
            return search(array, searchFor, lower, midIndex);
        }
    }
}
