package net.smallbulletin.recursionapp.util;

import java.util.List;

import jersey.repackaged.com.google.common.collect.Lists;

public class Knapsack {

    public static List<Integer> run(final List<Integer> items, final int target) {
        final List<Integer> nums = Lists.newArrayList();
        if (getNums(items, 0, target, nums)) {
            return nums;
        }
        return null;
    }

    private static boolean getNums(final List<Integer> items, final int remIndex, final int target,
            final List<Integer> nums) {
        if (target == 0) {
            return true;
        }

        if (remIndex >= items.size()) {
            return false;
        }

        final int num = items.get(remIndex);

        if (target - num < 0) {
            return getNums(items, remIndex + 1, target, nums);
        }

        if (getNums(items, remIndex + 1, target - num, nums)) {
            nums.add(num);
            return true;
        }

        return getNums(items, remIndex + 1, target, nums);
    }
}
