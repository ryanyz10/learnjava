package net.smallbulletin.bintree;

public class Tree<E extends Comparable<E>> {
    private TreeNode<E> root = null;

    public Tree() {

    }

    public TreeNode<E> getRoot() {
        return root;
    }

    public void insert(final E val) {
        insert(root, val);
    }

    private void insert(final TreeNode<E> sub, final E val) {
        if (root == null) {
            final TreeNode<E> toInsert = new TreeNode<>(val);
            root = toInsert;
            return;
        }

        if (val.compareTo(sub.getValue()) > 0) {
            if (sub.hasRight()) {
                insert(sub.getRight(), val);
            } else {
                final TreeNode<E> toInsert = new TreeNode<>(val);
                sub.setRight(toInsert);
                return;
            }
        } else {
            if (sub.hasLeft()) {
                insert(sub.getLeft(), val);
            } else {
                final TreeNode<E> toInsert = new TreeNode<>(val);
                sub.setLeft(toInsert);
                return;
            }
        }
    }

    public TreeNode<E> find(final E key) {
        return find(root, key);
    }

    private TreeNode<E> find(final TreeNode<E> sub, final E key) {
        if (sub == null) {
            return null;
        }
        final E currVal = sub.getValue();
        final int compare = key.compareTo(currVal);

        if (compare == 0) {
            return sub;
        } else if (compare < 0) {
            if (sub.getLeft() == null) {
                return null;
            }

            return find(sub.getLeft(), key);
        } else {
            if (sub.getRight() == null) {
                return null;
            }

            return find(sub.getRight(), key);
        }
    }

    public boolean contains(final E key) {
        return find(root, key) != null;
    }

    public void delete(final E key) {
        TreeNode<E> curr = root;
        TreeNode<E> parent = root;
        boolean onLeft = false;

        int compare = key.compareTo(curr.getValue());
        while (compare != 0) {
            parent = curr;

            if (compare < 0) {
                curr = curr.getLeft();
                onLeft = true;
            } else {
                curr = curr.getRight();
                onLeft = false;
            }

            if (curr == null) {
                return;
            }

            compare = key.compareTo(curr.getValue());
        }

        if (curr.isLeaf()) {
            if (curr == root) {
                curr = null;
            } else if (onLeft) {
                parent.setLeft(null);
            } else {
                parent.setRight(null);
            }
        } else if (curr.getRight() == null) { // on left
            if (curr == root) {
                root = curr.getLeft();
            } else if (onLeft) {
                parent.setLeft(curr.getLeft());
            } else {
                parent.setRight(curr.getLeft());
            }
        } else if (curr.getLeft() == null) { // on right
            if (curr == root) {
                root = curr.getRight();
            } else if (onLeft) {
                parent.setLeft(curr.getRight());
            } else {
                parent.setRight(curr.getRight());
            }

        } else {
            final TreeNode<E> newNodeParent = getSuccessorParent(curr);
            TreeNode<E> newNode = newNodeParent.getLeft();

            if (newNode == null) {
                newNode = newNodeParent;
            }

            final TreeNode<E> tmp = newNode.getRight();

            if (curr == root) {
                newNode.setLeft(root.getLeft());
                root = newNode;
            } else if (onLeft) {
                newNode.setRight(curr.getRight());
                newNode.setLeft(curr.getLeft());
                parent.setLeft(newNode);
            } else {
                newNode.setRight(curr.getRight());
                newNode.setLeft(curr.getLeft());
                parent.setRight(newNode);
            }

            if (newNode != root) {
                newNodeParent.setLeft(tmp);
            }

        }
    }

    private TreeNode<E> getSuccessorParent(final TreeNode<E> sub) {
        TreeNode<E> tmp = sub.getRight();
        TreeNode<E> parent = tmp;

        while (tmp.getLeft() != null) {
            parent = tmp;
            tmp = tmp.getLeft();
        }

        return parent;
    }

    public String inOrder() {
        return inOrder(root);
    }

    private String inOrder(final TreeNode<E> sub) {
        if (sub == null) {
            return "";
        }

        return inOrder(sub.getLeft()) + sub.getValue() + inOrder(sub.getRight());
    }

    public String preOrder() {
        return preOrder(root);
    }

    private String preOrder(final TreeNode<E> sub) {
        if (sub == null) {
            return "";
        }

        return sub.getValue() + preOrder(sub.getLeft()) + preOrder(sub.getRight());
    }

    public String postOrder() {
        return postOrder(root);
    }

    private String postOrder(final TreeNode<E> sub) {
        if (sub == null) {
            return "";
        }

        return postOrder(sub.getLeft()) + postOrder(sub.getRight()) + sub.getValue();
    }

    public String reverseOrder() {
        return reverseOrder(root);
    }

    private String reverseOrder(final TreeNode<E> sub) {
        if (sub == null) {
            return "";
        }

        return reverseOrder(sub.getRight()) + sub.getValue() + postOrder(sub.getLeft());
    }

    public TreeNode<E> max() {
        TreeNode<E> tmp = root;

        while (tmp.getRight() != null) {
            tmp = tmp.getRight();
        }

        return tmp;
    }

    public TreeNode<E> min() {
        TreeNode<E> tmp = root;

        while (tmp.getLeft() != null) {
            tmp = tmp.getLeft();
        }

        return tmp;
    }
}
