package net.smallbulletin.listapp.util;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.junit.Test;

public class SingleLinkedListTest {

	List<String> list = new SingleLinkedList<>();

	@Test
	public void testLinkedList() {
		SingleLinkedList<String> list = new SingleLinkedList<>();

		assertNotNull(list);
		assertEquals(0, list.size());
	}

	@Test
	public void testAddE() {

		String str = "abc";
		list.add(str);

		assertSame(str, list.get(0));
		assertEquals(1, list.size());
	}

	@Test
	public void testAddIntE() {

		String str = "abc";
		String str2 = "def";
		String str3 = "ghi";
		list.add(str2);
		list.add(str3);
		list.add(1, str);
		list.add(3, "jkl");
		assertSame(str2, list.get(0));
		assertSame(str, list.get(1));
		assertSame(str3, list.get(2));
		assertEquals(4, list.size());
		assertEquals("jkl", list.get(3));
	}

	@Test
	public void testAddAllCollectionOfQextendsE() {

		ArrayList<String> array = new ArrayList<>();
		array.add("123");
		array.add("456");

		list.addAll(array);

		for (int i = 0; i < array.size(); i++) {
			assertSame(array.get(i), list.get(i));
		}

		assertEquals(array.size(), list.size());
	}

	@Test
	public void testAddAllIntCollectionOfQextendsE() {

		list.add("100");
		list.add("101");
		list.add("102");

		ArrayList<String> array = new ArrayList<>();
		array.add("1");
		array.add("2");
		array.add("3");
		array.add("4");
		array.add("5");

		int index = 3;

		list.addAll(index, array);

		for (int i = 0; i < array.size(); i++) {
			assertSame(list.get(i + index), array.get(i));
		}

		assertEquals(8, list.size());
	}

	@Test
	public void testClear() {

		list.add("123");
		list.add("234");

		assertFalse(list.isEmpty());
		assertNotNull(list.get(0));

		list.clear();

		assertTrue(list.isEmpty());

	}

	@Test
	public void testContains() {

		String str = "135";

		list.add("123");
		list.add("234");
		list.add(str);
		list.add("345");

		assertTrue(list.contains(str));
		assertFalse(list.contains("abc"));
	}

	@Test
	public void testContainsAll() {
		ArrayList<String> array = new ArrayList<>();
		array.add("123");
		array.add("234");
		array.add("345");
		array.add("567");

		list.add("123");
		list.add("234");
		list.add("345");
		list.add("456");
		list.add("567");

		assertTrue(list.containsAll(array));

		array.add("789");

		assertFalse(list.containsAll(array));
	}

	@Test
	public void testGet() {

		String str = "abc";
		list.add(str);

		assertSame(str, list.get(0));

		str = "jkl";
		list.add("def");
		list.add("ghi");
		list.add(1, str);

		assertSame(str, list.get(1));
	}

	@Test
	public void testIndexOf() {
		int index = 2;
		String str = "123";

		list.add("234");
		list.add("345");
		list.add("456");
		list.add(index, str);

		assertSame(index, list.indexOf(str));
		assertEquals(0, list.indexOf("234"));
	}

	@Test
	public void testIsEmpty() {

		list.add("123");

		assertFalse(list.isEmpty());

		list.clear();

		assertTrue(list.isEmpty());
	}

	@Test
	public void testIterator() {

		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");

		Iterator<String> iter = list.iterator();
		assertEquals("1", iter.next());
		assertEquals("2", iter.next());
		assertEquals("3", iter.next());

		assertTrue(iter.hasNext());
		assertEquals("4", iter.next());
		assertEquals("5", iter.next());
		assertFalse(iter.hasNext());
		try {
			iter.next();
			fail("Expected null pointer exception");
		} catch (Exception e) {
		}
	}

	@Test
	public void testLastIndexOf() {

		String str = "123";
		int lastIndex = 5;

		list.add(str);
		list.add("234");
		list.add("345");
		list.add("456");
		list.add(str);
		list.add("567");
		list.add("678");
		list.add(lastIndex, str);

		assertSame(lastIndex, list.lastIndexOf(str));
	}

	@Test
	public void testListIterator() {

		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");

		ListIterator<String> iter = list.listIterator();

		assertEquals("1", iter.next());
		assertEquals("2", iter.next());
		assertEquals("3", iter.next());
		iter.remove();
		assertTrue(iter.hasNext());
		assertFalse(list.contains("3"));
		assertEquals("4", iter.next());
		assertEquals("5", iter.next());
		iter.set("6");
		assertFalse(list.contains("5"));
		assertTrue(list.contains("6"));
		assertFalse(iter.hasNext());
		//can't go backwards in our single link implementation, so add is useless
		iter.add("7");
		iter.add("8");
		assertEquals(6, list.size());
	}

	@Test
	public void testListIteratorInt() {

		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");

		ListIterator<String> iter = list.listIterator(1);

		assertEquals("2", iter.next());
		assertEquals("3", iter.next());
		iter.remove();
		assertTrue(iter.hasNext());
		assertFalse(list.contains("3"));
		assertEquals("4", iter.next());
		assertEquals("5", iter.next());
		iter.set("6");
		assertFalse(list.contains("5"));
		assertTrue(list.contains("6"));
		assertFalse(iter.hasNext());

		
		 iter.add("7");
		 iter.add("8");
		 assertEquals(6, list.size());
	}

	@Test
	public void testRemoveObject() {

		String str = "456";
		list.add("123");
		list.add("234");
		list.add("345");
		list.add(str);

		assertTrue(list.remove(str));
		assertEquals("345", list.get(2));
		assertEquals("234", list.get(1));
		assertEquals("123", list.get(0));
		assertFalse(list.contains(str));
		assertEquals(3, list.size());
	}

	@Test
	public void testRemoveInt() {

		String str = "456";
		int index = 3;
		list.add("123");
		list.add("234");
		list.add("345");
		list.add(index, str);

		assertEquals(str, list.remove(index));
		assertEquals("345", list.get(2));
		assertEquals("234", list.get(1));
		assertEquals("123", list.get(0));
		assertFalse(list.contains(str));
		assertEquals(3, list.size());
	}

	@Test
	public void testRemoveAll() {
		ArrayList<String> array = new ArrayList<>();

		array.add("234");
		array.add("345");
		array.add("567");

		list.add("123");
		list.add("234");
		list.add("345");
		list.add("456");
		list.add("567");

		list.removeAll(array);

		assertEquals(2, list.size());
		assertSame("123", list.get(0));
		assertSame("456", list.get(1));
	}

	@Test
	public void testRetainAll() {

		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");

		ArrayList<String> toRetain = new ArrayList<>();

		toRetain.add("2");
		toRetain.add("4");
		toRetain.add("100");

		list.retainAll(toRetain);

		assertEquals(2, list.size());

		assertEquals("2", list.get(0));
		assertEquals("4", list.get(1));
	}

	@Test
	public void testSet() {

		String str = "123";
		list.add("234");
		list.add("345");
		list.add("456");
		list.set(0, str);

		assertEquals(str, list.get(0));
		assertEquals(3, list.size());
	}

	@Test
	public void testSize() {

		assertEquals(0, list.size());
		list.add("123");
		assertEquals(1, list.size());
		list.add("234");
		assertEquals(2, list.size());
		list.add("345");
		assertEquals(3, list.size());
		list.remove(0);
		assertEquals(2, list.size());
	}

	@Test
	public void testSubList() {

		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");

		int start = 1;
		int end = 3;

		List<String> newList = list.subList(start, end);

		for (int i = start; i < end; i++) {
			assertEquals(list.get(i), newList.get(i - start));
		}
		assertEquals(end - start, newList.size());
	}

	@Test
	public void testToArray() {

		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");

		Object[] array = list.toArray();

		for (int i = 0; i < list.size(); i++) {
			assertEquals(list.get(i), array[i]);
		}

		assertEquals(list.size(), array.length);
	}

	@Test
	public void testToArrayTArray() {

		list.add("1");
		list.add("2");
		list.add("3");
		list.add("4");
		list.add("5");

		String[] array = new String[1];

		array = list.toArray(array);

		for (int i = 0; i < list.size(); i++) {
			assertEquals(list.get(i), array[i]);
		}

		assertEquals(list.size(), array.length);

		array = new String[5];

		array = list.toArray(array);

		for (int i = 0; i < list.size(); i++) {
			assertEquals(list.get(i), array[i]);
		}

		assertEquals(list.size(), array.length);

		final String[] original6 = new String[6];
		array = list.toArray(original6);
		assertSame(original6, array);

		int length = 0;

		int i = 0;

		while (i < array.length && array[i] != null) {
			length++;
			assertEquals(list.get(i), array[i]);
			i++;
		}

		assertEquals(list.size(), length);
	}

}
