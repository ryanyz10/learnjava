package net.smallbulletin.recursionapp.util;

import java.util.List;

import org.junit.Test;

public class HanoiTest {

    @Test
    public void testHanoi() {

        Hanoi hanoi = new Hanoi(2);

        hanoi.run();
        List<String> list = hanoi.getPics();
        for (String s : list) {
            System.out.print(s);
        }
    }

}
