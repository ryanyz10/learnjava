package net.smallbulletin.recursionapp.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class PowerTest {

    @Test
    public void testPower() {
        double n = Power.power(2, 5);
        assertEquals(32.0, n, 0);

        n = Power.power(2, 3);
        assertEquals(8.0, n, 0);

        n = Power.power(5, 4);
        assertEquals(625.0, n, 0);

        n = Power.power(2, -1);
        assertEquals(0.5, n, 0);

        n = Power.power(4, 0);
        assertEquals(1.0, n, 0);
    }

}
